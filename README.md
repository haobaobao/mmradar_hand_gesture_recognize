# mmradar_hand_gesture_recognize

B站演示视频：https://www.bilibili.com/video/BV1vJ411q7vS/

#### 介绍
使用HB100毫米波雷达模块做手势识别，本仓库主要包含三部分：  
1. Arduion的信号采集代码；  
2. 上位机代码；  
3. 信号采集与算法。  

#### 软件架构
本项目包含了软硬件设计，硬件部分请查看立创EDA开源项目[雷达手势识别](https://oshwhub.com/zhanghao/Radar_Gesture_Recognition)。  
Arduion程序主要实现了Ping-Pong缓冲区，采样雷达中频信号并通过串口上传至上位机。  
readData.py文件用于从下位机读取数据，并生成数据集；SVM.py文件用于训练SVM分类器。  
上位机使用QT编写，实时显示波形数据并输出分类结果。  

#### 使用说明

1.  硬件请参照立创EDA；
2.  该方法可实现4类（上、下、挥手、推拉）手势的识别，平均准确率在95%以上；
3.  软件部署方法请自行探索，如果您对此十分感兴趣，也可联系zhanghao190@mails.ucas.ac.cn，我将提供更详细的资料，欢迎大家讨论。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
